﻿// Install the C# / .NET helper library from twilio.com/docs/csharp/install

using ExamLogin;
using System;
using System.Collections.Generic;
using System.Linq;
using Twilio;
using Twilio.Rest.Api.V2010.Account;


class Program
{

    private static void DrawMenu(int maxitems)
    {

        Console.WriteLine(" 1. Register");
        Console.WriteLine(" 2. Recover");
        Console.WriteLine(" 3. Exit program");
        Console.WriteLine("Make your choice: type 1, 2,... or {0} for exit", maxitems);

    }
    static int GenerateRandomNo()
    {
        int _min = 1000;
        int _max = 9999;
        Random _rdm = new Random();
        return _rdm.Next(_min, _max);
    }
    static void Main(string[] args)
    {
        // Find your Account Sid and Token at twilio.com/console
        const string accountSid = "ACd96860a323f1078af2379df0db5a713d";
        const string authToken = "66080a4a984b44da6b1235aa3006c338";

        TwilioClient.Init(accountSid, authToken);

        //var message = MessageResource.Create(
        //    body: "Beka 4ooooort.",
        //    from: new Twilio.Types.PhoneNumber("+18558208804"),
        //    to: new Twilio.Types.PhoneNumber("+77771691055")
        //);

        //Console.WriteLine(message.Sid);



        const int maxMenuItems = 3;
        int selector = 0;
        bool good = false;
        List<User> users;

        while (selector != maxMenuItems)
        {
            Console.Clear();

            DrawMenu(maxMenuItems);
            good = int.TryParse(Console.ReadLine(), out selector);
            if (good)
            {
                switch (selector)
                {
                    case 1:
                        using (var context = new ContextSms())
                        {
                            users = context.Users.ToList();

                            Console.WriteLine("Enter Login: ");
                            string tempLogin = Console.ReadLine();
                            Console.WriteLine("Enter Phone number (+7XXXXXXXXXX): ");
                            string tempPhone = Console.ReadLine();

                            var selectedUsers = from user in users
                                                where user.PhoneNumber == tempPhone
                                                select user;

                            //var phone = context.Users.Where(p => p.PhoneNumber == tempPhone);
                            if(selectedUsers.Any())
                            {
                                Console.WriteLine("Such number already used. Try to recover");
                                Console.ReadLine();
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Start");
                                int code = GenerateRandomNo();
                                
                                var message = MessageResource.Create(
                                    body: code.ToString(),
                                    from: new Twilio.Types.PhoneNumber("+18558208804"),
                                    to: new Twilio.Types.PhoneNumber(tempPhone)
                                );
                                Console.WriteLine("Enter code:");
                                int tempcode = Convert.ToInt32(Console.ReadLine());
                                if (tempcode == code) {
                                    context.Users.Add(new User
                                    {
                                        Name = tempLogin,
                                        Login = tempLogin,
                                        PhoneNumber = tempPhone

                                    });
                                    Console.WriteLine("Welcome");
                                    context.SaveChanges();
                                    Console.ReadLine();
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("Wrong code");
                                }
                            }


                        }
                        Console.ReadKey();
                        break;

                        case 2:
                        using (var context = new ContextSms())
                        {
                            users = context.Users.ToList();
                            string tempLogin = Console.ReadLine();
                            Console.WriteLine("Enter Phone number (+7XXXXXXXXXX) to recover: ");
                            string tempPhone = Console.ReadLine();

                            var selectedUsers = from user in users
                                                where user.PhoneNumber == tempPhone
                                                select user;


                            if (selectedUsers.Any())
                            {
                                Console.WriteLine("Start");
                                int code = GenerateRandomNo();

                                var message = MessageResource.Create(
                                    body: code.ToString(),
                                    from: new Twilio.Types.PhoneNumber("+18558208804"),
                                    to: new Twilio.Types.PhoneNumber(tempPhone)
                                );
                                Console.WriteLine("Enter code:");
                                int tempcode = Convert.ToInt32(Console.ReadLine());
                                if (tempcode == code)
                                {
                                    Console.WriteLine("Welcome back");
                                   
                                    Console.ReadLine();
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("Wrong code");
                                }

                            }
                            else
                            {
                                Console.WriteLine("Such number is not registered.");
                                Console.ReadLine();
                                break;
                            }
                                context.SaveChanges();
                        }
                        Console.ReadKey();
                        break;


                    default:
                        break;

                }
            }
        }

        //using (var context = new ContextSms())
        //{
        //    var result = context.Users.ToList();
        //    context.Users.Add(new User
        //    {
        //        Name = "John",
        //        Login = "Black",
        //        PhoneNumber = "+77774410041"


        //    });

        //    context.SaveChanges();
        //}
    }
}
